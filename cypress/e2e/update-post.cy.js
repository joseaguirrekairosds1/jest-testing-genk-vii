describe('Should Update Post', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/');

    cy.get('sidebar-posts')
      .shadow()
      .find('nav ul li')
      .first()
      .find('post-ui')
      .shadow()
      .find('a')
      .click({ position: 'top' });

    cy.get('update-post')
      .shadow()
      .find('#title')
      .clear()
      .focus()
      .type('Update Post');

    cy.get('update-post')
      .shadow()
      .find('#content')
      .clear()
      .focus()
      .type('Update Content');

    cy.get('update-post')
      .shadow()
      .find('.button')
      .eq(1)
      .should('have.text', 'Update')
      .click({ position: 'top' });

    cy.wait(1500);

    cy.get('sidebar-posts')
      .shadow()
      .find('nav ul li')
      .first()
      .find('post-ui')
      .shadow()
      .find('a')
      .should('have.text', 'Update Post');

  })
})