describe('Should Add Post', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/');

    cy.get('sidebar-posts')
      .shadow()
      .find('add-button')
      .shadow()
      .find('.add-button')
      .click({ position: 'top' });

    cy.get('create-post')
      .shadow()
      .find('#title')
      .focus()
      .type('New Post');

    cy.get('create-post')
      .shadow()
      .find('#content')
      .focus()
      .type('New Content');

    cy.get('create-post')
      .shadow()
      .find('.button')
      .eq(1)
      .should('have.text', 'Add')
      .click({ position: 'top' });

    cy.wait(1500);

    cy.get('sidebar-posts')
      .shadow()
      .find('nav ul li')
      .first()
      .find('post-ui')
      .shadow()
      .find('a')
      .should('have.text', 'New Post');
  })
})