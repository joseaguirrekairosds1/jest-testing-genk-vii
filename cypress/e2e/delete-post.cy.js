describe('Should Delete Post', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/');

    cy.get('sidebar-posts')
      .shadow()
      .find('nav ul li')
      .first()
      .find('post-ui')
      .shadow()
      .find('a')
      .click({ position: 'top' });


    cy.get('update-post')
      .shadow()
      .find('.button')
      .eq(2)
      .should('have.text', 'Delete')
      .click({ position: 'top' });

    cy.on('window:confirm', () => true);

    cy.wait(1500);

    cy.get('sidebar-posts')
      .shadow()
      .find('nav ul li')
      .first()
      .find('post-ui')
      .shadow()
      .find('a')
      .should('have.text', 'qui est esse');

  })
})