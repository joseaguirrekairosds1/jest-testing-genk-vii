import "./main.css";
import "./components/header.component";
import "./components/sidebar.component";
import { Router } from "@vaadin/router";

import "./pages/home.page";
import "./pages/posts.page";

const router = new Router(document.getElementById('outlet'));

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/(post[s]?)/:id", component: "posts-page" },
  { path: "(.*)", redirect: "/" },
]);
