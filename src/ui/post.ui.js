import { LitElement, html, css } from "lit";

export class PostUI extends LitElement {

  constructor() {
    super();
    this.post = {};
    this.selected = '';
  }

  static get styles() {
    return css`
      :host{
        display: block;
        max-width: 100%;
        margin: 0.5rem;
      }
      .button {
          display: inline-block;
          text-align: left;
          text-decoration: none;
          font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
          font-size: 1rem;
          width: 95%;
          background-color: #eaeaea;
          padding: 0.4rem;
          margin: 0px;
          border: none;
          border-radius: 4px;
          cursor: pointer;
      }
      .button:active {
        color: black;
      }
      .button:hover:not(.post-selected) {
          background-color: #d7d7d7;
      }
      .post-selected {
        background-color: #c55e19;
        color: white;
      }
  `;
  }

  static get properties() {
    return {
      post: { type: Object },
      selected: { type: String }
    };
  }

  render() {
    return html`
      <a class="button ${this.selected === this.post.id ? 'post-selected' : ''}" .href="/posts/${this.post.id}">${this.post.title}</a>
    `;
  }
}

customElements.define("post-ui", PostUI);
