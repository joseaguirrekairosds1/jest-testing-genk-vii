import { LitElement, html, css } from "lit";

export class AddButtonUI extends LitElement {

  static get styles() {
    return css`
      .button-container {
        width: 100%;
        text-align: right;
      }
      .add-button {
        display: inline-block;
        text-align: center;
        text-decoration: none;
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 1rem;
        width: 8rem;
        background-color: #d17232;
        color: white;
        padding: 0.4rem;
        margin: 0.6rem 0.6rem 0px 0px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
      }
      .add-button:hover {
        background-color: #c55e19;
      }
    `;
  }

  render() {
    return html`
      <div class="button-container">
        <a href="/" class="add-button">Agregar Post</a>
      </div>
    `;
  }
}

customElements.define("add-button", AddButtonUI);
