import { LitElement, html, css } from "lit";
import { AddPostUseCase } from "../usecases/add-post.usecase";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";

export class CreatePost extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
        border: black solid 2px;
      }
      .form {
        margin: 1rem;
      }
      label {
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-weight: 600;
      }
      input[type=text], textarea {
        width: 100%;
        padding: 12px 20px;
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 1rem;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
      }

      .buttons-container {
        display: flex;
        background: none;
        padding: 0.5rem;
      }

      .button {
        display: inline-block;
        text-align: center;
        text-decoration: none;
        font-size: 1rem;
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        width: 80%;
        background-color: #d17232;
        color: white;
        padding: 0.5rem;
        margin: 0.5rem;
        border: none;
        border-radius: 4px;
        cursor: pointer;
      }

      .button:hover {
        background-color: #c55e19;
      }

      div {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
      }
    `;
  }

  constructor() {
    super();
    this.postId = 0;
    this.posts = [];
    this.post = {};
  }

  static get properties() {
    return {
      postId: { type: Number },
      posts: { type: Array },
      post: { type: Object }
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.postId = Number(location.pathname.split("/").pop());
    this.posts = JSON.parse(sessionStorage.getItem("posts"));
    if (!this.posts || this.posts.length <= 0) {
      this.posts = await AllPostsUseCase.execute();
      sessionStorage.setItem("posts", JSON.stringify(this.posts));
    }
  }

  updateTitleValue(e) {
    this.post.title = e.srcElement.value.trim();
  }

  updateContentValue(e) {
    this.post.content = e.srcElement.value.trim();
  }

  async createPost() {
    if (!!this.post.title) {
      this.posts = await AddPostUseCase.execute(this.posts, this.post);
      this.post = this.posts[0];
      sessionStorage.setItem("posts", JSON.stringify(this.posts));
      location.href = `/posts/${this.post.id}`;
    }
  }

  clearForm() {
    this.post = {};
    let titleImput = this.renderRoot.querySelector('#title');
    let contentImput = this.renderRoot.querySelector('#content');
    titleImput.value = '';
    contentImput.value = '';
  }

  render() {
    return html`
      <div class="form">
        <label for="title">Title</label>
        <input type="text" id="title" name="title" @change=${this.updateTitleValue}>

        <label for="content">Content</label>
        <textarea type="text" id="content" name="content" rows="4" cols="50" @change=${this.updateContentValue}></textarea>
      
        <div class="buttons-container">
          <button @click="${this.clearForm}" class="button">Cancel</button>
          <button @click="${this.createPost}" class="button">Add</button>
        </div>
  </div>
    `;
  }
}

customElements.define("create-post", CreatePost);
