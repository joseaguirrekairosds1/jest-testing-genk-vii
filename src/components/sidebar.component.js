import { LitElement, html, css } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import "./../ui/add-button.ui";
import "./../ui/post.ui";

export class PostsComponent extends LitElement {

  constructor() {
    super();
    this.posts = [];
    this.idSelected = '';
  }

  static get styles() {
    return css`
      :host {
        display: block;
        border: black solid 2px;
        margin: 0.5rem;
        width: 100%;
      }
      ul {
        padding: 0px;
      }
    `;
  }

  static get properties() {
    return {
      posts: { type: Array },
      idSelected: { type: String }
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = JSON.parse(sessionStorage.getItem("posts"));
    if (!this.posts || this.posts?.length <= 0) {
      this.posts = await AllPostsUseCase.execute();
      sessionStorage.setItem("posts", JSON.stringify(this.posts));
    }
    const observeUrlChange = () => {
      const body = document.querySelector("body");
      const observer = new MutationObserver((_) => {
        let href = document.location.href;
        this.idSelected = Number(href.split("/").pop());
      });
      observer.observe(body, { childList: true, subtree: true });
    };
    window.onload = observeUrlChange;

  }

  render() {
    return html`
        <add-button></add-button>
        <nav><ul>
          ${this.posts?.map(
      (post) => html`<li><post-ui .selected="${this.idSelected}" .post="${post}"></post-ui></li>`
    )}
        </ul></nav>
    `;
  }
}

customElements.define("sidebar-posts", PostsComponent);
