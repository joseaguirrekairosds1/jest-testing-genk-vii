import { LitElement, html, css } from "lit";
import logoKairos from '../assets/logo-k.png';

export class HeaderComponent extends LitElement {
  constructor() {
    super();
    this.subtitle = 'POSTS'
  }

  static get styles() {
    return css`
      :host {
        display: block;
        border: black solid 2px;
      }
      header {
        margin: 1rem;
      }
      header a img  {
        height: 1.5rem;
      }
      header h3 {
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        margin: 0.5rem 0px;
      }
    `;
  }

  static get properties() {
    return {
      subtitle: { type: String },
    };
  }

  render() {
    return html`
        <header>
          <a href="/">
            <img .src="${logoKairos}" alt="Kairos Logo">
          </a>
          <h3>${this.subtitle}</h3>
        </header>
    `;
  }
}

customElements.define("header-posts", HeaderComponent);
