import "../components/update-post.component";

export class PostsPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <update-post></update-post>
    `;
  }
}

customElements.define("posts-page", PostsPage);
