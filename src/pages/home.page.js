import "../components/create-post.component";

export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <create-post></create-post>
    `;
  }
}

customElements.define("home-page", HomePage);
